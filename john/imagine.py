from __future__ import (absolute_import, division, print_function)
from .lennon import Piano
from beatles.george.harrison import Sitar
from george.harrison import StringInstrument

class Song():

	def __init__(self):
		pass

	def perform(self):
		si = StringInstrument()
		si.play()
		s = Sitar()
		s.play()
		l = Piano()
		l.play()
		print ("imagine all the people")