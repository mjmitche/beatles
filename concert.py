import george.harrison

import paul.mccartney

import john.lennon

import ringo.starr

from john import imagine

def cli():
	g = george.harrison.Sitar()
	g.play()

	p = paul.mccartney.BassGuitar()
	p.play()

	r = ringo.starr.Drums()
	r.play()

	l = john.lennon.Guitar()
	l. play()

	song = imagine.Song()
	song.perform()



if __name__ == '__main__':
	cli()